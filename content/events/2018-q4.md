---
id: 2018-q4
date: 2018-10-27
title: Is Christ Physically Present in the Lord’s Supper?
banner: dark/wood
moderator: Conley Owens
panelists:
  - name: Steve Roma
    title: Pastor
    org: Resurrection Lutheran Church
    orgurl: http://www.resurrectionsantaclara.com/
    location: Santa Clara
    position: affirmative
  - name: Calvin Goligher
    title: Pastor
    org: First Orthodox Presbyterian Church
    orgurl: https://firstopc.org/
    location: Sunnyvale
    position: negative
youtube: iR9Zb5lfCk4
rsvp: https://www.eventbrite.com/e/silicon-valley-areopagus-forum-october-panel-discussion-tickets-50517136114
---

Is Jesus present in the Lord's supper?  If so, how is he present?  What does
this mean for us practically?  A comparison of the Lutheran and Reformed views.
