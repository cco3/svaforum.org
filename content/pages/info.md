---
title: Information for Panelists
banner: dark/halfdome
---

<div class="p-3"></div>

<div class="infolistset">
<div class="infolist">

### Format

1.  Panels will consist of two panelists with differing views.
1.  The schedule:
    1.  60 minutes of moderated discussion prompted by questions asked by the
        host
    1.  15 minute break
    1.  60 minutes of moderated discussion prompted by questions asked by the
        audience (mediated by the host)
    1.  15 minute break
    1.  Dinner in an environment designed to continue the discussion between
        audience members
1.  Panelists will be addressed on a first name basis (Due to the diversity of
    belief systems we anticipate hosting, the SVAF does not endorse any
    particular title held by the panelists).
1.  No corporate prayers will be offered at the event (once again, due to the
    diversity of belief systems we anticipate hosting).

</div>

<div class="infolist">

### Decorum

1.  A primary objective of SVAF is to model healthy discussion, so panelists
    are expected to express their disagreements graciously.
1.  Panelists are expected to refrain from any language that would not be
    considered family friendly.
1.  Recommended attire is business casual (although clerical garb or other
    attire is perfectly fine).

### Planning

1.  The event will be scheduled on a Saturday based on the availability of the
    two panelists.
1.  Panelists will be requested to provide a headshot, soon after a date is
    confirmed.
1.  Panelists will be requested to send a bio (max 100 words) prior to the
    event.
1.  Panelists should arrive 10 minutes early for a mic check.

</div>

<div class="infolist">

### Other

1.  Events will be recorded.
1.  Photography/videography is permitted, provided it is not obtrusive.
1.  No honorarium is provided for the event.
1.  No literature distribution is permitted beyond any conducted by SVAF or the
    host church.

</div>
</div>
