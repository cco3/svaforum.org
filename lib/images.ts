import fs from 'fs/promises'
import path from 'path'

import { globby } from 'globby'
import jpeg from 'jpeg-js'
import { publicDir } from './projpath'

export interface ImageInfo {
  path: string
  size: number
  width: number
  height: number
}

export async function readImage (p: string): Promise<ImageInfo> {
  const content = await fs.readFile(p)
  const stat = await fs.stat(p)

  return {
    size: stat.size,
    path: p,
    ...jpeg.decode(content)
  }
}

export async function readImages (root: string, p: string): Promise<ImageInfo[]> {
  root = publicDir(root)
  const images = []
  for (p of await globby(path.join(root, p))) {
    const img = await readImage(p)
    img.path = path.relative(root, img.path)
    images.push(img)
  }
  return images
}
