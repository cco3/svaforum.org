import dayjs from 'dayjs'
import { computed } from 'vue'
import { PanelEvent } from '~/lib/events'

export default () => {
  const now = useNow()
  return computed(() => (e: PanelEvent) =>
    now.value < dayjs(e.date, 'America/Los_Angeles').add(1, 'day'))
}
