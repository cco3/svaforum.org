import dayjs from 'dayjs'
import { computed, onMounted } from 'vue'
import { useRoute } from 'vue-router'

export default () => {
  const now = useState('now', () => 0)
  const route = useRoute()
  const updateNow = () => {
    let d = route?.query?.date
    if (Array.isArray(d)) {
      d = d[0] ?? null
    }
    now.value = dayjs(d, 'America/Los_Angeles').valueOf()
  }
  updateNow()
  // Using onMounted is necessary to make sure all dependant variables are
  // updated after hydration.
  onMounted(() => {
    updateNow()
    setInterval(updateNow, 60 * 1000)
  })
  return computed(() => dayjs(now.value))
}
